<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
<h1>Full Address</h1>
<p><?php echo getFullAddress("Philippines", "Davao City", "Davao Del Sur", "312 Avenue"); ?></p>

<h1>Grades</h1>
<p><?php echo getLetterGrade(100); ?></p>
<p><?php echo getLetterGrade(77); ?></p>

</body>
</html>